
<?php 
session_start();
require_once 'inc/connection.php'; 
require_once 'inc/class.validation.php';
require_once 'inc/functions.php';

$query = mysqli_query($db,"SELECT * FROM crimes r INNER JOIN criminals c ON r.criminal_id = c.criminal_id WHERE c.file_no ='$_GET[file]'");
$rows = mysqli_fetch_array($query);

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <style type="text/css">
     .header{
            width:100%;
            margin-left:200px;
        }
     .cads{
            font-style: normal;
            font-size: 2.9em;
            color: #99CCFF;
            text-decoration: underline;
            text-shadow: 2px 2px 2px gray;
        }
      .remainder{
            font-style: normal;
            font-size: 1.2em;
            color: purple;
            position: relative;
            top: -15px;
            left: -20px;
            text-shadow: 2px 2px 2px gray;
        }
        .has-error{
          border-color: #dd4b39;
          box-shadow: none;
        }
  </style>
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-yellow layout-top-nav">
<div class = "header">
    <img src="img/namelogo.png">
</div>
<div class="wrapper">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="dashboard.php" class="navbar-brand"></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
       <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="home.php"><i class="fa fa-home"></i> Home <span class="sr-only">(current)</span></a></li>
            <li class="active"><a href="report.php"><i class="fa fa-edit"></i> Report Crime</a></li>
            <li><a href="changepass.php"><i class="fa fa-pencil"></i> Change Password</a></li>
            <li><a href="logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
       
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <br>
   
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-warning">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="photos/<?php echo $rows['photo']; ?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $rows['firstname']."".$rows['lastname']; ?></h3>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Age</b> <a class="pull-right"><?php echo $rows['age'] ?></a>
                </li>
                <li class="list-group-item">
                  <b>Address</b> <a class="pull-right"><?php echo $rows['address']; ?></a>
                </li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="box box-warning">
            <div class="box-body box-profile">
              <h3>Crimes</h3>
              <ul class="list-group list-group-unbordered">
                <?php 
                $crom = mysqli_query($db,"SELECT * FROM crimes WHERE criminal_id ='$rows[criminal_id]'");
                  while ($row = mysqli_fetch_array($crom)) {
                 ?>
                <li class="list-group-item">
                  <b><?php echo $row['crime_type'] ?></b> <a style="margin-left: 230px;" href="evidence/<?php echo $row['evidence'] ?>"> <?php echo $row['evidence'] ?></a> <a class="pull-right"> <?php echo $row['date'] ?></a>
                </li>
               <?php
             }
               ?>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
      <!--   <b>Version</b> 2.4.0 -->
      </div>
      <strong>Copyright &copy; 2014-2016 <a href=""></a>.</strong> 
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript">
  $(document).ready(()=>{
     $('#fname').focusout(function() {
      $value = $('#fname').val();
       $len = $value.length;
       if ($value === "") {
        $('#fname').addClass('has-error');
        $('#fname_text').text('First Name is Required');
        document.getElementById('save').disabled = true;
       }else
  if ($value.match(/^[0-9]*$/)) {
    $('#fname_text').text('First Name  must be character only');
    document.getElementById('save').disabled = true;
  }else
  if ($len < 3) 
    {
  $('#fname').addClass('has-error');
    $('#fname_text').text('First Name is too short character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  if ($len > 20) 
    {
  $('#fname').addClass('has-error');
    $('#fname_text').text('First Name is too long character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  {
    $('#fname').removeClass('has-error').addClass('has-success');
    $('#fname_text').text('').removeClass('has-error');
    document.getElementById('save').disabled = false;
  }
     });
  $('#lname').focusout(function() {
      $value = $('#lname').val();
       $len = $value.length;
       if ($value === "") {
        $('#lname').addClass('has-error');
        $('#lname_text').text('Last Name is Required');
        document.getElementById('save').disabled = true;
       }else
  if ($value.match(/^[0-9]*$/)) {
    $('#lname_text').text('Last Name  must be character only');
    document.getElementById('save').disabled = true;
  }else
  if ($len < 3) 
    {
  $('#lname').addClass('has-error');
    $('#fname_text').text('Last Name is too short character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  if ($len > 20) 
    {
  $('#lname').addClass('has-error');
    $('#lname_text').text('Last Name is too long character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  {
    $('#lname').removeClass('has-error').addClass('has-success');
    $('#lname_text').text('').removeClass('has-error');
    document.getElementById('save').disabled = false;
  }
     });
    $('#phone').focusout(function(){

  $value  = $('#phone').val();
  $len = $value.length;
  if($value === ""){
    $('#phone').addClass('has-error');
    $('#phone_text').text('Phone Number is required');
    document.getElementById('save').disabled = true;
  }else
  if ($len != 11) 
    {
  $('#phone').addClass('has-error');
    $('#phone_text').text('Phone Number must be 11 digits');
    document.getElementById('Save').disabled = true;
  }else
  if(isNaN($value)){
$('#phone').addClass('has-error');
    $('#phone_text').text('Phone must be digits');
    document.getElementById('submit').disabled = true;
  }else{
    $('#phone').removeClass('has-error').addClass('has-success');
    $('#phone_text').text('').removeClass('has-error');
    document.getElementById('save').disabled = false;
  }
});
$('#email').focusout(function() {
      $value = $('#email').val();
       $len = $value.length;
       if ($value === "") {
        $('#email').addClass('has-error');
        $('#email_text').text('Email is Required');
        document.getElementById('save').disabled = true;
       }else
  if ($len < 3) 
    {
  $('#email').addClass('has-error');
    $('#email_text').text('Email is too short character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  {
    $('#email').removeClass('has-error').addClass('has-success');
    $('#email_text').text('').removeClass('has-error');
    document.getElementById('save').disabled = false;
  }
     });
$('#rank').focusout(function() {
      $value = $('#rank').val();
       $len = $value.length;
       if ($value === "") {
        $('#rank').addClass('has-error');
        $('#rank_text').text('Rank is Required');
        document.getElementById('save').disabled = true;
       }else
  if ($value.match(/^[0-9]*$/)) {
    $('rank_text').text('Rank  must be character only');
    document.getElementById('save').disabled = true;
  }else
  if ($len < 3) 
    {
  $('#rank').addClass('has-error');
    $('#rank_text').text('Rank is too short character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  if ($len > 20) 
    {
  $('#rank').addClass('has-error');
    $('#rank_text').text('Rank is too long character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  {
    $('#rank').removeClass('has-error').addClass('has-success');
    $('rank_text').text('').removeClass('has-error');
    document.getElementById('save').disabled = false;
  }
     });
$('#username').focusout(function() {
      $value = $('#username').val();
       $len = $value.length;
       if ($value === "") {
        $('#username').addClass('has-error');
        $('#username_text').text('Username is Required');
        document.getElementById('save').disabled = true;
       }else
  if ($len < 3) 
    {
  $('#username').addClass('has-error');
    $('#username_text').text('Username is too short character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  if ($len > 20) 
    {
  $('#username').addClass('has-error');
    $('#username_text').text('Username is too long character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  {
    $('#username').removeClass('has-error').addClass('has-success');
    $('#username_text').text('').removeClass('has-error');
    document.getElementById('save').disabled = false;
  }
     });
    $('#send').click(()=>{
     var file = $('#file_no').val();
     var fname = $('#fname').val();
     var lname = $('#lname').val();
     var age = $('#age').val();   
     var date = $('#date').val();
     var address = $('#address').val();
     var crime = $('#crime').val();
     var page ='fresh';
     $.ajax({
      type: 'POST',
      url: 'Ajax.handler.php',
      data:{page:page,file:file,fname:fname,lname:lname,age:age,date:date,address:address,crime:crime},
      success: function(x) {
        $('#status').html(x);
      }
     })
    })
  })
</script>
</body>
</html>

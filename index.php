<?php include 'inc/config.php'; ?>
<?php include "inc/connection.php"; ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/class.validation.php'; ?>
<?php include "inc/function.php"; 
session_start();
?>

<!-- Login Container -->
<div id="login-container">
    <!-- Login Header -->
    <h1 class="h2 text-light text-center push-top-bottom animation-slideDown">
        <img src="img/logo-big.png" alt="">
    </h1>
    <!-- END Login Header -->

    <!-- Login Block -->
    <div class="block animation-fadeInQuickInv">
        <!-- Login Title -->
        <div class="block-title">
            <h2>Please Login</h2>
        </div>
        <!-- END Login Title -->

        <!-- Login Form -->
        <form id="form-login" action="index.php" method="post" class="form-horizontal">
        <?php 
        if (isset($_POST['login-submit'])) {
            $username = $_POST['login-email'];
            $password = md5($_POST['login-password']);
            $fields = array(
                array('name'=>'login-email',
                      'app_name' => 'Username',
                      'isRequired' => true
                     ),
                 array('name'=>'login-password',
                      'app_name' => 'Password',
                      'isRequired' => true
                     )
            );
$Validation = new Validation($fields,'POST');
if($Validation->out == 1) {
     $query = mysqli_query($db,"SELECT * FROM admin WHERE username='$username' AND password='$password'");
                $row = mysqli_fetch_array($query);
     $uquery = mysqli_query($db,"SELECT * FROM users WHERE username='$username' AND password='$password'");
                $urow = mysqli_fetch_array($uquery);
                //print_r($row);
                if ($row > 0) {
                    $_SESSION['admin'] = $row['username'];
                    header('Location:dashboard.php');
                }elseif ($urow > 0) {
                     $_SESSION['uuser'] = $urow['username'];
                     $_SESSION['uuser_id'] = $urow['user_id'];
                       header('Location:home.php');
                }

        }
}

         ?>
            <div class="form-group">
                <div class="col-xs-12">
                    <input type="text" name="login-email" class="form-control" placeholder="Your Username..">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <input type="password" id="login-password" name="login-password" class="form-control" placeholder="Your password..">
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-xs-8">
                    <label class="csscheckbox csscheckbox-primary">
                        <input type="checkbox" id="login-remember-me" name="login-remember-me">
                        <span></span>
                    </label>
                    Remember Me?
                </div>
                <div class="col-xs-4 text-right">
                    <button type="submit" name="login-submit" class="btn btn-effect-ripple btn-sm btn-primary"><i class="fa fa-check"></i> Login</button>
                </div>
            </div>
        </form>
        <!-- END Login Form -->
    </div>
    <!-- END Login Block -->

    <!-- Footer -->
    <footer class="text-muted text-center animation-pullUp">
        <small><span id="year-copy"></span> &copy; <a href="" target="_blank"><?php echo $template['name'] . ' ' . $template['version']; ?></a></small>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Login Container -->

<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->

<?php include 'inc/template_end.php'; ?>
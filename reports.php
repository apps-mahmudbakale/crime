
<?php 
session_start();
require_once 'inc/connection.php'; 
require_once 'inc/class.validation.php';
require_once 'inc/functions.php';
// $id = $_GET['id'];

// $row = mysqli_fetch_array(mysqli_query($db,"SELECT * FROM criminals a INNER JOIN crimes b ON a.criminal_id = b.criminal_id WHERE a.criminal_id = '$id'"));

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <style type="text/css">
     .header{
            width:100%;
            margin-left:200px;
        }
     .cads{
            font-style: normal;
            font-size: 2.9em;
            color: #99CCFF;
            text-decoration: underline;
            text-shadow: 2px 2px 2px gray;
        }
      .remainder{
            font-style: normal;
            font-size: 1.2em;
            color: purple;
            position: relative;
            top: -15px;
            left: -20px;
            text-shadow: 2px 2px 2px gray;
        }
        .has-error{
          border-color: #dd4b39;
          box-shadow: none;
        }
  </style>
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-yellow layout-top-nav">
<div class = "header">
    <img src="img/namelogo.png">
</div>
<div class="wrapper">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="dashboard.php" class="navbar-brand"></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="dashboard.php"><i class="fa fa-home"></i> Home <span class="sr-only">(current)</span></a></li>
            <li><a href="users.php"><i class="fa fa-users"></i> Users</a></li>
            <li><a href="reports.php"><i class="fa fa-table"></i> Reports</a></li>
            <li><a href="adminChangepass.php"><i class="fa fa-pencil"></i> Change Password</a></li>
            <li><a href="logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <br>
      <div class="col-lg-12">
      <div class="panel panel-warning" style='border-radius:0'>
        <div class="panel-heading"><i class='fa fa-table'></i> Crimes Report</div>
        <div class="panel-body">
          <table class='table table-striped col-lg-12'>
            <thead>
               <tr>
                   <th>S/N</th>
                   <th>FILE NO</th>
                   <th>FIRSTNAME</th>
                   <th>LASTNAME</th>
                   <th>ACTIONS</th>
                </tr>
            </thead>
               <tbody>
                 <?php
                 $sn = 0;
                 $query = mysqli_query($db, "SELECT * FROM criminals");
                 while($row = mysqli_fetch_array($query)) {
                   $sn++;
                   echo "<tr>";
                     echo "<td>".$sn."</td>";
                     echo "<td>".$row['file_no']."</td>";
                     echo "<td>".$row['firstname']."</td>";
                     echo "<td>".$row['lastname']."</td>";
                     echo "<td>".$row['age']."</td>";

                   echo "</tr>";
                 }
                ?>
               </tbody>
                
            </table>
        </div>
        <div class='panel-footer'>
           
        </div>
      </div>
    </div>
       <div class="col-lg-6">
      <div class="panel panel-warning" style='border-radius:0'>
        <div class="panel-heading"><i class='fa fa-table'></i> Crimes Report</div>
        <div id="status">
          <?php
          $crime = array();
          $rimez = array(); 
          $value = array();
           $type = mysqli_query($db,"SELECT DISTINCT(crime_type) FROM crimes");
           while ($rows = mysqli_fetch_array($type)) {
            $row = "'".$rows['crime_type']."'";
             array_push($crime, $row);
             array_push($rimez, $rows['crime_type']);
           }
           foreach ($rimez as $key) {
             $num = mysqli_query($db,"SELECT * FROM crimes WHERE crime_type ='$key'");
             $no = mysqli_num_rows($num);
             array_push($value, $no);
           }
           // print_r($crime);
           // print_r($value);
           $crimes = trim(implode(',', $crime),',');
           $values = trim(implode(',', $value),',');
           ?>

        </div>

<div class="panel-body">
<script src="code/highcharts.js"></script>
<script src="../../code/highcharts-more.js"></script>
<script src="../../code/modules/exporting.js"></script>

<div id="container"></div>

<?php $g ="Crime Chart" ?>

    <script type="text/javascript">

var chart = Highcharts.chart('container', {

    title: {
        text: '<?php echo $g; ?>'
    },

    subtitle: {
        text: 'Percentages of Crimes'
    },

    xAxis: {
        categories: [<?php echo $crimes ?>]
    },

    series: [{
        type: 'column',
        colorByPoint: true,
        data: [<?php echo $values ?>],
        showInLegend: false
    }]

});


$('#plain').click(function () {
    chart.update({
        chart: {
            inverted: false,
            polar: false
        },
        subtitle: {
            text: ''
        }
    });
});

$('#inverted').click(function () {
    chart.update({
        chart: {
            inverted: true,
            polar: false
        },
        subtitle: {
            text: 'Inverted'
        }
    });
});

$('#polar').click(function () {
    chart.update({
        chart: {
            inverted: false,
            polar: true
        },
        subtitle: {
            text: 'Polar'
        }
    });
});

    </script>
        </div>
        <div class='panel-footer'>
           
        </div>
      </div>
    </div>
           <div class="col-lg-6">
      <div class="panel panel-warning" style='border-radius:0'>
        <div class="panel-heading"><i class='fa fa-table'></i> Crimes Report</div>
        <div id="status">
          <?php
          $a = mysqli_query($db,"SELECT * FROM criminals WHERE age BETWEEN 10 AND 20");
          $ar = mysqli_num_rows($a);
          $b = mysqli_query($db,"SELECT * FROM criminals WHERE age BETWEEN 21 AND 30");
          $br = mysqli_num_rows($b);
           $c = mysqli_query($db,"SELECT * FROM criminals WHERE age BETWEEN 31 AND 40");
           $cr = mysqli_num_rows($c);
           ?>

        </div>

<div class="panel-body">
<script src="code/highcharts.js"></script>
<script src="../../code/highcharts-more.js"></script>
<script src="../../code/modules/exporting.js"></script>

<div id="containerr"></div>
<script type="text/javascript">
Highcharts.chart('containerr', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        }
    },
    title: {
        text: 'criminals Report Based Age Range'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
    },
    series: [{
        type: 'pie',
        name: 'Age Ranges',
        data: [
            ['Age 10-20', <?php echo $ar ?>],
            ['Age 21-30', <?php echo $br ?>],
            ['Age 31-40', <?php echo $cr?>]
        ]
    }]
});
    </script>
        </div>
        <div class='panel-footer'>
           
        </div>
      </div>
    </div>
    </div>
    </div>
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
      <!--   <b>Version</b> 2.4.0 -->
      </div>
      <strong>Copyright &copy; 2014-2016 <a href=""></a>.</strong> 
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript">
  $(document).ready(()=>{
     $('#fname').focusout(function() {
      $value = $('#fname').val();
       $len = $value.length;
       if ($value === "") {
        $('#fname').addClass('has-error');
        $('#fname_text').text('First Name is Required');
        document.getElementById('save').disabled = true;
       }else
  if ($value.match(/^[0-9]*$/)) {
    $('#fname_text').text('First Name  must be character only');
    document.getElementById('save').disabled = true;
  }else
  if ($len < 3) 
    {
  $('#fname').addClass('has-error');
    $('#fname_text').text('First Name is too short character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  if ($len > 20) 
    {
  $('#fname').addClass('has-error');
    $('#fname_text').text('First Name is too long character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  {
    $('#fname').removeClass('has-error').addClass('has-success');
    $('#fname_text').text('').removeClass('has-error');
    document.getElementById('save').disabled = false;
  }
     });
  $('#lname').focusout(function() {
      $value = $('#lname').val();
       $len = $value.length;
       if ($value === "") {
        $('#lname').addClass('has-error');
        $('#lname_text').text('Last Name is Required');
        document.getElementById('save').disabled = true;
       }else
  if ($value.match(/^[0-9]*$/)) {
    $('#lname_text').text('Last Name  must be character only');
    document.getElementById('save').disabled = true;
  }else
  if ($len < 3) 
    {
  $('#lname').addClass('has-error');
    $('#fname_text').text('Last Name is too short character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  if ($len > 20) 
    {
  $('#lname').addClass('has-error');
    $('#lname_text').text('Last Name is too long character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  {
    $('#lname').removeClass('has-error').addClass('has-success');
    $('#lname_text').text('').removeClass('has-error');
    document.getElementById('save').disabled = false;
  }
     });
    $('#phone').focusout(function(){

  $value  = $('#phone').val();
  $len = $value.length;
  if($value === ""){
    $('#phone').addClass('has-error');
    $('#phone_text').text('Phone Number is required');
    document.getElementById('save').disabled = true;
  }else
  if ($len != 11) 
    {
  $('#phone').addClass('has-error');
    $('#phone_text').text('Phone Number must be 11 digits');
    document.getElementById('Save').disabled = true;
  }else
  if(isNaN($value)){
$('#phone').addClass('has-error');
    $('#phone_text').text('Phone must be digits');
    document.getElementById('submit').disabled = true;
  }else{
    $('#phone').removeClass('has-error').addClass('has-success');
    $('#phone_text').text('').removeClass('has-error');
    document.getElementById('save').disabled = false;
  }
});
$('#email').focusout(function() {
      $value = $('#email').val();
       $len = $value.length;
       if ($value === "") {
        $('#email').addClass('has-error');
        $('#email_text').text('Email is Required');
        document.getElementById('save').disabled = true;
       }else
  if ($len < 3) 
    {
  $('#email').addClass('has-error');
    $('#email_text').text('Email is too short character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  {
    $('#email').removeClass('has-error').addClass('has-success');
    $('#email_text').text('').removeClass('has-error');
    document.getElementById('save').disabled = false;
  }
     });
$('#rank').focusout(function() {
      $value = $('#rank').val();
       $len = $value.length;
       if ($value === "") {
        $('#rank').addClass('has-error');
        $('#rank_text').text('Rank is Required');
        document.getElementById('save').disabled = true;
       }else
  if ($value.match(/^[0-9]*$/)) {
    $('rank_text').text('Rank  must be character only');
    document.getElementById('save').disabled = true;
  }else
  if ($len < 3) 
    {
  $('#rank').addClass('has-error');
    $('#rank_text').text('Rank is too short character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  if ($len > 20) 
    {
  $('#rank').addClass('has-error');
    $('#rank_text').text('Rank is too long character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  {
    $('#rank').removeClass('has-error').addClass('has-success');
    $('rank_text').text('').removeClass('has-error');
    document.getElementById('save').disabled = false;
  }
     });
$('#username').focusout(function() {
      $value = $('#username').val();
       $len = $value.length;
       if ($value === "") {
        $('#username').addClass('has-error');
        $('#username_text').text('Username is Required');
        document.getElementById('save').disabled = true;
       }else
  if ($len < 3) 
    {
  $('#username').addClass('has-error');
    $('#username_text').text('Username is too short character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  if ($len > 20) 
    {
  $('#username').addClass('has-error');
    $('#username_text').text('Username is too long character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  {
    $('#username').removeClass('has-error').addClass('has-success');
    $('#username_text').text('').removeClass('has-error');
    document.getElementById('save').disabled = false;
  }
     });
  $('#send').click(()=>{
     var id = <?php echo $id;?>;
     var date = $('#date').val();
     var crime = $('#crime').val();
     var age = $('#age').val();
     var page ='returning';
     $.ajax({
      type: 'POST',
      url: 'Ajax.handler.php',
      data:{page:page,date:date,crime:crime,age:age,id:id},
      success: function(x) {
        $('#status').html(x);
      }
     })
    })
  })
</script>
</body>
</html>

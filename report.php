
<?php 
session_start();
require_once 'inc/connection.php'; 
require_once 'inc/class.validation.php';
require_once 'inc/functions.php';
if (isset($_GET['id'])) {
  $query = mysqli_query($db,"SELECT criminal_id FROM criminals WHERE file_no='".$_GET[id]."' ");
  $row = mysqli_fetch_array($query);
  $id = $row['criminal_id'];
  mysqli_query($db,"DELETE FROM criminals WHERE criminal_id='$id'");
  mysqli_query($db,"DELETE FROM crimes WHERE criminal_id='$id'");
  echo "<script>alert('Record Deleted');window.location='report.php';</script>";
}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <style type="text/css">
     .header{
            width:100%;
            margin-left:200px;
        }
     .cads{
            font-style: normal;
            font-size: 2.9em;
            color: #99CCFF;
            text-decoration: underline;
            text-shadow: 2px 2px 2px gray;
        }
      .remainder{
            font-style: normal;
            font-size: 1.2em;
            color: purple;
            position: relative;
            top: -15px;
            left: -20px;
            text-shadow: 2px 2px 2px gray;
        }
        .has-error{
          border-color: #dd4b39;
          box-shadow: none;
        }
  </style>
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-yellow layout-top-nav">
<div class = "header">
    <img src="img/namelogo.png">
</div>
<div class="wrapper">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="dashboard.php" class="navbar-brand"></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="home.php"><i class="fa fa-home"></i> Home <span class="sr-only">(current)</span></a></li>
            <li class="active"><a href="report.php"><i class="fa fa-edit"></i> Report Crime</a></li>
            <li><a href="changepass.php"><i class="fa fa-pencil"></i> Change Password</a></li>
            <li><a href="logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
       
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <br>
       <div class="col-lg-12">
      <div class="panel panel-warning" style='border-radius:0'>
        <div class="panel-heading"><i class='fa fa-folder-open'></i> New Crime</div>
        <div id="status"></div>
          <div class="panel-body"> 
                 <div class="col-lg-6">
                  File Number
                   <input type="text" id="file_no" value="<?php echo randomPass(); ?>" class="form-control">
                   <span id="file_no_text" style="color: #dd4b39"></span>
                 </div>
                 <div class="col-lg-6">
                  <br>
                  <button id="go"  class='btn btn-success'><i class="fa fa-arrow-right"></i> Go</button>
                 </div>
        </div>
        <div class='panel-footer'>
           <!--  <button id="save"  class='btn btn-success'><i class="fa fa-save"></i> Save</button> -->
           <!--  <a href="" class="btn btn-success pull-right">Batch Upload Doctors</a> -->
        </div>
         
      </div>
    </div>
        <div class="col-lg-12">
      <div class="panel panel-warning" style='border-radius:0'>
        <div class="panel-heading"><i class="fa fa-table"></i> Crimes</div>
        <div class="panel-body">
            <table class='table table-striped col-lg-12'>
            <thead>
               <tr>
                   <th>S/N</th>
                   <th>FILE NO</th>
                   <th>FIRSTNAME</th>
                   <th>LASTNAME</th>
                   <th>ACTIONS</th>
                </tr>
            </thead>
               <tbody>
                 <?php
                 $sn = 0;
                 $query = mysqli_query($db, "SELECT * FROM criminals");
                 while($row = mysqli_fetch_array($query)) {
                   $sn++;
                   echo "<tr>";
                     echo "<td>".$sn."</td>";
                     echo "<td>".$row['file_no']."</td>";
                     echo "<td>".$row['firstname']."</td>";
                     echo "<td>".$row['lastname']."</td>";
                      echo "<td><a href='view_criminal.php?file=".$row['file_no']."' class='btn btn-default'><i class='fa fa-eye'></i></a><a href='?id=".$row['file_no']."' class='btn btn-default'><i class='fa fa-trash'></i></a></td>";
                   echo "</tr>";
                 }
                ?>
               </tbody>
                
            </table>
        </div>
      </div>
    </div> 
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
      <!--   <b>Version</b> 2.4.0 -->
      </div>
      <strong>Copyright &copy; 2014-2016 <a href=""></a>.</strong> 
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript">
  $(document).ready(()=>{
     $('#file_no').focusout(function() {
      $value = $('#file_no').val();
       $len = $value.length;
       if ($value === "") {
        $('#file_no').addClass('has-error');
        $('#file_no_text').text('File Number is Required');
        document.getElementById('go').disabled = true;
       }else
  if (isNaN($value)) {
    $('#file_no_text').text('File Number  must be character only');
    document.getElementById('go').disabled = true;
  }else
  if ($len < 3) 
    {
  $('#file_no').addClass('has-error');
    $('#file_no_text').text('File Number is too short character length must be between 3 and 20');
    document.getElementById('go').disabled = true;
  }
  else
  if ($len > 20) 
    {
  $('#file_no').addClass('has-error');
    $('#file_no_text').text('File Number is too long character length must be between 3 and 20');
    document.getElementById('go').disabled = true;
  }
  else
  {
    $('#file_no').removeClass('has-error').addClass('has-success');
    $('#file_no_text').text('').removeClass('has-error');
    document.getElementById('go').disabled = false;
  }
     });
    $('#go').click(()=>{
     var file = $('#file_no').val();
     var page ='report';
     $.ajax({
      type: 'POST',
      url: 'Ajax.handler.php',
      data:{page:page,file:file},
      success: function(x) {
        $('#status').html(x);
      }
     })
    })
  })
</script>
</body>
</html>
